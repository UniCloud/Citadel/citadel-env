# Citadel

Citadel Secure OS

# upstream
## Host security hardening
https://docs.openstack.org/openstack-ansible/latest/reference/architecture/security.html
'''OpenStack-Ansible provides a comprehensive security hardening role that applies over 200 security configurations as recommended by the Security Technical Implementation Guide (STIG) provided by the Defense Information Systems Agency (DISA). These security configurations are widely used and are distributed in the public domain by the United States government.'''

### role:
https://docs.openstack.org/ansible-hardening/latest/